#include <stdio.h>

int main(){
    char nama[20] = "Hendra Lijaya";
    int umur = 18;
    float berat = 50.7;
    double tinggi = 169.73;
    char jenis_kelamin = 'L';

    printf("Nama saya adalah %s\n",nama);
    printf("Umur saya %d tahun\n",umur);
    printf("Berat badan saya adalah %.2f\n",berat);
    printf("Tinggi badan saya adalah %.2f\n",tinggi);
    printf("Jenis kelamin saya adalah %c",jenis_kelamin);
    return 0;
}